import axios from 'axios'
import axiosRetry from 'axios-retry'

axiosRetry(axios, {
  retries: 8,
  retryDelay: (retryCount) => {
    return retryCount * 1000
  },
})

// Add a request interceptor
axios.interceptors.request.use(
  (config) => {
    const token = sessionStorage.getItem('vue-session-key')
      ? JSON.parse(sessionStorage.getItem('vue-session-key')).tokenSession
      : ''
    config.baseURL = process.env.VUE_APP_AUTH_API
    config.headers.get['Accept'] = 'application/json'
    config.headers.get['Content-Type'] = 'application/json'
    config.headers.get['X-Requested-With'] = 'XMLHttpRequest'
    config.headers.get['Access-Control-Allow-Origin'] = '*'
    config.headers.get['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    config.headers.get['Access-Control-Allow-Headers'] = 'Origin, Content-Type, X-Requested-With, Accept'
    config.headers.post['Accept'] = 'application/json'
    config.headers.post['Content-Type'] = 'application/json'
    config.headers.post['X-Requested-With'] = 'XMLHttpRequest'
    config.headers.post['Access-Control-Allow-Origin'] = '*'
    config.headers.post['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    config.headers.post['Access-Control-Allow-Headers'] = 'Origin, Content-Type, X-Requested-With, Accept'
    config.headers.common['Authorization'] = 'Bearer ' + token
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default axios
