const Projects = [
  {
    username: 'Dessie',
    ammount: '$ 15.00',
    name: 'Compra Orange-u #04885-1',
    deadline: '2 days later',
    progress: 90,
    color: 'pink',
  },
  {
    username: 'Jakayla',
    ammount: '$ 5.00',
    name: 'Compra Orange-u #04886-1',
    deadline: '1 weeks later',
    progress: 70,
    color: 'success',
  },
  {
    username: 'Ludwiczakpawel',
    ammount: '$ 45.00',
    name: 'Compra Orange-u #04887-1',
    deadline: '1 Month later',
    progress: 50,
    color: 'info',
  },
]

const getProject = (limit) => {
  return limit ? Projects.slice(0, limit) : Projects
}

export { Projects, getProject }
