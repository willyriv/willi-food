import Vue from 'vue'
import router from './router/'
import store from './store/'
import axios from './api'
import VueSession from 'vue-session'
import './registerServiceWorker'
import './plugins/storage'
import './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'font-awesome/css/font-awesome.css'
import './theme/default.sass'

import App from './App.vue'
import vuetify from './plugins/vuetify'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(VueSession)
Vue.config.productionTip = false

axios.defaults.timeout = 60000
axios.defaults.baseURL = process.env.VUE_APP_AUTH_API
// Add a response interceptor
axios.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201 || response.status === 204) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response)
    }
  },
  (error) => {
    if (error.response && error.response.status) {
      switch (error.response.status) {
        case 401: {
          if (error.response.config.url == axios.defaults.baseURL + 'auth/login') {
            Vue.swal({
              icon: 'error',
              title: 'Datos inválidos',
              html: 'La combinación de correo electrónico y contraseña es incorrecta',
              showCancelButton: false,
              showConfirmButton: true,
            })
          }

          if (
            sessionStorage.getItem('vue-session-key') &&
            JSON.parse(sessionStorage.getItem('vue-session-key')).tokenSession
          ) {
            Vue.swal({
              icon: 'error',
              title: `Acceso restringido`,
              html: `Tu sesión ha caducado o no tienes permisos suficientes para continuar.`,
              showCancelButton: false,
              showConfirmButton: true,
            })
            sessionStorage.removeItem('vue-session-key')
            router.push('/')
          }

          throw new Error(error)
        }

        case 422: {
          let contentMessage = ''
          let arrayOfErrors = []
          if (error.response.data.errors) {
            contentMessage =
              '<p>Por favor, verifique la información suministrada e intente nuevamente</p><dl style="text-align: justify">'
            arrayOfErrors = Object.entries(error.response.data.errors)
            arrayOfErrors.forEach((error) => {
              contentMessage = contentMessage + '<dt style="text-transform: capitalize">' + error[0] + '</dt>'
              error[1].forEach((item) => {
                contentMessage = contentMessage + '<dd>- ' + item + '</dd >'
              })
            })
            contentMessage = contentMessage + '</dl>'
          } else {
            contentMessage = 'La información sumistrada es inválida'
          }
          Vue.swal({
            icon: 'error',
            title: error.response.data.message ? error.response.data.message : 'Ocurrió un error',
            html: contentMessage,
            showCancelButton: false,
            showConfirmButton: true,
          })
          throw new Error(error)
        }
      }
    }
    return Promise.reject(error)
  }
)

window.axios = axios
Vue.prototype.$axios = axios
const app = new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
})

app.$mount('#app')
