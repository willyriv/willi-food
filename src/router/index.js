import Vue from 'vue'
import Router from 'vue-router'
import axios from '@/api'
import { publicRoute, protectedRoute } from './config'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
const routes = publicRoute.concat(protectedRoute)

Vue.use(Router)
const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: routes,
})
// router gards
router.beforeEach((to, from, next) => {
  NProgress.start()

  // Gets connection country
  let country = {}
  if (!sessionStorage.getItem('selectedCountry')) {
    axios.get('https://api.franchise-lead.com/api/get-location').then((response) => {
      country = response.data
      sessionStorage.setItem('selectedCountry', JSON.stringify(response.data))

      // Validate authentication
      if (to.path != 'login' && to.meta.requiresAuth) {
        const authUser = JSON.parse(sessionStorage.getItem('vue-session-key')) // Gets user information
        let counter = 0
        let assignedRoles = []
        if (authUser && authUser.roles) {
          authUser.roles.forEach((role) => {
            assignedRoles.push(role.name)
          })
        }
        if (authUser && authUser.tokenSession) {
          // If token exist and is required
          to.meta.requiredRoles.forEach((role) => {
            if (assignedRoles.includes(role)) counter++
          })
          if (counter > 0) {
            next({ props: { country: country, access: 'permitted' } })
          } else {
            next({ path: from.path, props: { country: country, access: 'denied' } })
          }
        } else {
          // If token doesn't exist but is required
          next({ path: 'login', query: { redirect: to.path } })
        }
      } else {
        next({ props: { country: country } })
      }
    })
  } else {
    country = sessionStorage.getItem('selectedCountry')
    // Validate authentication
    if (to.path != 'login' && to.meta.requiresAuth) {
      const authUser = JSON.parse(sessionStorage.getItem('vue-session-key')) // Gets user information
      let counter = 0
      let assignedRoles = []
      if (authUser && authUser.roles) {
        authUser.roles.forEach((role) => {
          assignedRoles.push(role.name)
        })
      }
      if (authUser && authUser.tokenSession) {
        // If token exist and is required
        to.meta.requiredRoles.forEach((role) => {
          if (assignedRoles.includes(role)) counter++
        })
        if (counter > 0) {
          next({ props: { country: country, access: 'permitted' } })
        } else {
          next({ path: from.path, props: { country: country, access: 'denied' } })
        }
      } else {
        // If token doesn't exist but is required
        next({ path: 'login', query: { redirect: to.path } })
      }
    } else {
      next({ props: { country: country } })
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})

export default router
